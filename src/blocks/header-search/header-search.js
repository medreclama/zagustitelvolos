const headerSearch = () => {
  const search = document.querySelector('.header-search');
  const form = search.querySelector('.header-search__form');
  search.addEventListener('click', (event) => {
    const { target } = event;
    if (target.tagName === 'A') {
      event.preventDefault();
      if (target.classList.contains('header-search__open')) {
        form.classList.add('header-search__form--visible');
        setTimeout(() => search.classList.add('header-search--active'), 10);
      }
      if (target.classList.contains('header-search__close')) {
        search.classList.remove('header-search--active');
        setTimeout(() => form.classList.remove('header-search__form--visible'), 200);
      }
    }
  });
};

export default headerSearch;
