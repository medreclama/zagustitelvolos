import Swiper, { Navigation, Pagination } from 'swiper';

const homeSlider = () => new Swiper('.home-slider__swiper', {
  modules: [Navigation, Pagination],
  direction: 'horizontal',
  loop: false,
  spaceBetween: 40,
  watchSlidesProgress: true,
  speed: 500,
  grabCursor: true,

  breakpoints: {
    480: {
      slidesPerView: 2,
    },
    800: {
      slidesPerView: 3,
    },
    1000: {
      slidesPerView: 4,
    },
  },

  navigation: {
    nextEl: '.home-slider__button--next',
    prevEl: '.home-slider__button--prev',
  },

  pagination: {
    el: '.swiper-pagination',
    clickable: true,
    dynamicBullets: true,
  },
});

export default homeSlider;
