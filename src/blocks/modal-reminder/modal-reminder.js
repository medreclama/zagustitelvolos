import { showModal } from '../modal/modal';

class ModalReminder {
  #timer;
  #time = 45000;
  #timeSecondary = 10000;

  #showModal = () => {
    showModal(this.modal);
    this.resetTimer();
    localStorage.removeItem('addedToCartTime');
  };

  #setTimer = (time) => {
    this.#timer = setTimeout(() => {
      this.#showModal();
    }, time);
  };

  constructor() {
    this.modal = document.querySelector('#modal-reminder');
  }

  resetTimer = () => {
    clearTimeout(this.#timer);
    localStorage.removeItem('addedToCartTime');
  };

  init = () => {
    if (localStorage.getItem('addedToCartTime')) {
      const time = this.#time - (Date.now() - +localStorage.getItem('addedToCartTime'));
      const timerValue = time > 0 ? time : this.#timeSecondary;
      this.#setTimer(timerValue);
    }
    window.addEventListener('addedToCart', () => {
      this.resetTimer();
      localStorage.setItem('addedToCartTime', `${Date.now()}`);
      this.#setTimer(this.#time);
    });
  };
}

export default ModalReminder;
