import { showModal, closeModal } from '../modal/modal';

const modalCity = document.getElementById('modal-city');
const headerCity = document.querySelector('.header-top__city-name');
const modalCityList = document.querySelectorAll('.modal-city__link');

headerCity.addEventListener('click', (evt) => {
  evt.preventDefault();
  showModal(modalCity);
});

const changeCity = () => {
  modalCityList.forEach((city) => {
    city.addEventListener('click', (evt) => {
      evt.preventDefault();
      headerCity.innerHTML = evt.target.innerHTML;
      localStorage.setItem('cityName', evt.target.innerHTML);
      closeModal(modalCity);
    });
  });
};

document.addEventListener('DOMContentLoaded', () => {
  const localStorageValue = localStorage.getItem('cityName');
  if (localStorageValue) {
    headerCity.innerHTML = localStorageValue;
  }
});

export default changeCity;
