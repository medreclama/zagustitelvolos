const addStarGradient = (container, rating) => {
  container.forEach((star, i) => {
    if (i < Math.floor(rating)) star.style.setProperty('--gradient-stop', '100%');
    else if (i === Math.floor(rating)) star.style.setProperty('--gradient-stop', `${(rating - Math.floor(rating)) * 100}%`);
  });
};

const feedbackItemRating = () => {
  const blocks = Array.from(document.querySelectorAll('.feedback-item'));
  let sum = 0;
  blocks.forEach((block) => {
    const stars = Array.from(block.querySelectorAll('.feedback-item__rating-star'));
    const rating = +block.dataset.rating;
    if (!rating) return;
    addStarGradient(stars, rating);
    sum += Number(block.dataset.rating);
  });
  // Общий рейтинг
  const average = sum / (blocks.length);
  const ratingSum = document.querySelector('.rating-sum');
  if (!ratingSum) return;
  if (!average) ratingSum.style.display = 'none';
  const starsSum = Array.from(ratingSum.querySelectorAll('.rating-sum__star'));
  addStarGradient(starsSum, average);
};

const feedbackItem = () => {
  feedbackItemRating();
};

export default feedbackItem;
