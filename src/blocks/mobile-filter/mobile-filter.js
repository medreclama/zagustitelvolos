const sort = document.querySelector('.sort');
const mobileFilter = document.querySelector('.mobile-filter');

const addFilter = () => {
  if (!(sort && mobileFilter)) return;
  const filterButtons = mobileFilter.querySelectorAll('.mobile-filter__button');
  mobileFilter.addEventListener('click', (evt) => {
    const clickedButton = evt.target;
    if (clickedButton.classList.contains('mobile-filter__button')) {
      if (clickedButton.closest('div').classList.contains('mobile-filter__block--active')) {
        clickedButton.closest('div').classList.remove('mobile-filter__block--active');
        clickedButton.classList.remove('mobile-filter__button--active');
      } else {
        filterButtons.forEach((button) => {
          button.closest('div').classList.remove('mobile-filter__block--active');
          button.classList.remove('mobile-filter__button--active');
        });
        clickedButton.closest('div').classList.add('mobile-filter__block--active');
        clickedButton.classList.add('mobile-filter__button--active');
      }
    }
  });
};

export default addFilter;
