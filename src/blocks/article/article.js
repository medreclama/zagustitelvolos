const article = document.querySelector('.article');

const showMoreContent = () => {
  const contentLinkShow = article.querySelector('.article-content__all');
  const contentItems = Array.from(article.querySelectorAll('.article-content__link'));
  const LINKS_NUM = 5;
  const contentHiddenItems = contentItems.slice(LINKS_NUM, contentItems.length);
  const showLink = () => {
    contentHiddenItems.forEach((contentHiddenItem) => {
      const item = contentHiddenItem;
      item.style.display = 'none';
      item.classList.add('hidden');
      contentLinkShow.addEventListener('click', (evt) => {
        evt.preventDefault();
        if (item.classList.contains('hidden')) {
          contentLinkShow.innerHTML = 'Скрыть';
          item.style.display = 'block';
        } else {
          contentLinkShow.innerHTML = 'Показать все';
          item.style.display = 'none';
        }
        item.classList.toggle('hidden');
      });
    });
  };

  if (contentItems.length > (LINKS_NUM)) {
    showLink();
  } else if (contentLinkShow) {
    contentLinkShow.style.display = 'none';
  }
};

const readArticle = () => {
  if (!article) return;
  showMoreContent();
};

export default readArticle;
