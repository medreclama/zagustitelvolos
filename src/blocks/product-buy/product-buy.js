export default () => {
  const productValue = document.querySelector('.product-buy__value');
  if (productValue) {
    const field = document.querySelector('.product-buy__field');
    const changers = Array.from(document.querySelectorAll('.product-buy__change'));
    changers.forEach((item) => {
      item.addEventListener('click', (e) => {
        e.preventDefault();
        if (e.target.dataset.valueChange === 'inc' && (+field.value < field.max || !field.max)) field.value = +field.value + 1;
        if (e.target.dataset.valueChange === 'dec' && (+field.value > field.min || !field.min)) field.value = +field.value - 1;
      });
    });
  }
};
