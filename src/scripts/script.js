import fancybox from '@fancyapps/fancybox'; // eslint-disable-line no-unused-vars
import 'img-comparison-slider';
import likely from 'ilyabirman-likely';
import sh from '../blocks/show-hide/show-hide';
import { tabs } from '../blocks/tabs/tabs';
import headerNavigationSublistSwitcher from '../blocks/header-navigation/header-navigation';
import headerMobileSwitcher from '../blocks/header-mobile-switcher/header-mobile-switcher';
import productBuy from '../blocks/product-buy/product-buy';
import headerSearch from '../blocks/header-search/header-search';
import counters from './modules/counters';
import modal from '../blocks/modal/modal';
import ModalReminder from '../blocks/modal-reminder/modal-reminder';
import homeSlider from '../blocks/home-slider/home-slider';
import changeCity from '../blocks/modal-city/modal-city';
import readArticle from '../blocks/article/article';
import addFilter from '../blocks/mobile-filter/mobile-filter';
import feedbackItem from '../blocks/feedback-item/feedback-item';
import video360Init from '../blocks/video-js/video-js';

const countersCodeHead = `
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script data-skip-moving="true" async src="https://www.googletagmanager.com/gtag/js?id=UA-63628127-1"></script>
  <script data-skip-moving="true">
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-63628127-1');
      gtag('config', 'AW-973893218');
  </script>
  <!-- /Global site tag (gtag.js) -->
  
  <script data-skip-moving="true">
      function gtag_report_conversion(url) {
          var callback = function () {
              if (typeof(url) != 'undefined') {
                  window.location = url;
              }
          };
          gtag('event', 'conversion', {
              'send_to': 'AW-973893218/cLspCNX-k44DEOLcsdAD',
              'event_callback': callback
          });
          return false;
      }
  </script>

  <script type="text/javascript">
    var __cs = __cs || [];
    __cs.push(["setAccount", "t6W9ECdGHyxwtdLrwDYFlETzFGmK5csx"]);
    __cs.push(["setHost", "//server.comagic.ru/comagic"]);
  </script>

  <script type="text/javascript">
    var yaParams = {/*Здесь параметры визита*/};
  </script>

  <script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>
  <script type="text/javascript" data-skip-moving="true">
    !function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?169",t.onload=function(){VK.Retargeting.Init("VK-RTRG-1192124-cHIug"),VK.Retargeting.Hit()},document.head.appendChild(t)}();
  </script>
  <noscript><img src="https://vk.com/rtrg?p=VK-RTRG-1192124-cHIug" style="position:fixed; left:-999px;" alt=""/></noscript>
`;
const countersCodeBody = `
  <!-- Yandex.Metrika counter -->
  <script type="text/javascript" >
      (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
          m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
      (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
  
      ym(30658727, "init", {
          clickmap:true,
          trackLinks:true,
          accurateTrackBounce:true,
          webvisor:true,
          ecommerce:"dataLayer"
      });
  </script>
  <noscript><div><img src="https://mc.yandex.ru/watch/30658727" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <!-- /Yandex.Metrika counter -->
  
  <script type="text/javascript">
      jQuery(document).ready(function($){
          $('form#ORDER_FORM').submit(function(e){
              yaCounter30658727.reachGoal('Order');
          });
  
          $('.bx_catalog_item_controls a').click(function(e){
              yaCounter30658727.reachGoal('GOAL_ADD2BASKET');
          });
      });
  </script>
`;

counters(countersCodeHead, 'head');
counters(countersCodeBody);

sh();
tabs();
headerNavigationSublistSwitcher();
headerMobileSwitcher();
productBuy();
headerSearch();
modal();
homeSlider();
changeCity();
likely.initiate();
readArticle();
addFilter();
feedbackItem();
const modalReminder = new ModalReminder();

modalReminder.init();
window.modalReminder = modalReminder;

document.addEventListener('AfterGiftsRequest', homeSlider);

// Скрипт для якорной ссылки feedback-form формы в табе "Отзывы" в карточке товара
if (window.location.href.indexOf('feedback') > -1) {
  const feedbackForm = document.getElementById('feedback-form');
  const feedbackTab = document.getElementById('tab-feedback');
  const labels = document.querySelectorAll('.tabs__label');
  const items = document.querySelectorAll('.tabs__item');
  labels.forEach((label, i) => {
    label.classList.remove('tabs__label--active');
    if (label.id === 'tab-feedback') {
      items[0].classList.remove('tabs__item--active');
      feedbackTab.classList.add('tabs__label--active');
      items[i].classList.add('tabs__item--active');
      feedbackForm.scrollIntoView();
    }
  });
}

video360Init();
