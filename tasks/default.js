import gulp from 'gulp';

gulp.task('default', gulp.series('stylus', 'template', 'script', 'copy', 'webp', 'svgSprites', 'server', 'watcher'));
gulp.task('build', gulp.parallel('stylus', 'template', 'script', 'copy', 'webp', 'svgSprites'));
